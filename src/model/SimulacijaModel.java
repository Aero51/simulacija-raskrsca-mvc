/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.math.BigDecimal;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.SingleSelectionModel;

/**
 *
 * @author Nikola
 */
public class SimulacijaModel {
   //public class SimulacijaModel implements Serializable{ 
  //  private Integer vrijemeOtvorenosti,vrijemeZatvorenosti;
 private BigDecimal vrijemeOtvorenosti,vrijemeZatvorenosti;
   
    public BigDecimal getVrijemeOtvorenosti() {
        return vrijemeOtvorenosti;
    }

   
    public void setVrijemeOtvorenosti(BigDecimal vrijemeOtvorenosti) {
        this.vrijemeOtvorenosti = vrijemeOtvorenosti;
    }

  
    public BigDecimal getVrijemeZatvorenosti() {
        return vrijemeZatvorenosti;
    }

   
    public void setVrijemeZatvorenosti(BigDecimal vrijemeZatvorenosti) {
        this.vrijemeZatvorenosti = vrijemeZatvorenosti;
    }
    
      public double minVozila = 0.0;

 
  public double maxVozila = 15.0;

 
  public IntegerProperty selektiranaVozilaIstok = new SimpleIntegerProperty(0);
 public IntegerProperty selektiranaVozilaSjever = new SimpleIntegerProperty(0);
 public IntegerProperty selektiranaVozilaZapad = new SimpleIntegerProperty(0);
 
  public BooleanProperty automatski = new SimpleBooleanProperty(false);

  
 

 
 // public SingleSelectionModel modSelectionModel;
  
  //napravit  getter i setter 
 
 
  
    
   public void addListenerToModSelectionModelIstok(ChangeListener listener) {
    
      selektiranaVozilaIstok.addListener(listener);
        

    }
  
  public void addListenerToModSelectionModelSjever(ChangeListener listener) {
    
      selektiranaVozilaSjever.addListener(listener);
        

    }
   public void addListenerToModSelectionModelZapad(ChangeListener listener) {
    
      selektiranaVozilaZapad.addListener(listener);
        

    }
}
