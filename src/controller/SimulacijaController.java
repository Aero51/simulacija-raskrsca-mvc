/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static java.lang.Math.abs;
import java.math.BigDecimal;
import java.util.ArrayList;
import javafx.animation.Animation.Status;
import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.animation.TimelineBuilder;
import javafx.animation.TranslateTransition;
import javafx.animation.TranslateTransitionBuilder;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import jfxtras.labs.scene.control.gauge.Semafor;
import jfxtras.labs.scene.control.gauge.SemaforPjesaci;
import jfxtras.labs.scene.control.gauge.Skretaci;
import model.SimulacijaModel;
import view.SimulacijaMainView;

/**
 *
 * @author Nikola
 */
public class SimulacijaController {

    private SimulacijaModel appM;
   
    
    private SimulacijaMainView appMainV;
    private ChangeListener listenerOnChangeSlideVozilaIstok, listenerOnChangeSlideVozilaSjever, listenerOnChangeSlideVozilaZapad;
    private EventHandler<javafx.scene.input.MouseEvent> listenerOnMenuPress, listenerOnScenePress, listenerPromjenePrihvati, listenerTipkaloButton, listenerOnSlideIstokReleased, listenerOnSlideSjeverReleased, listenerOnSlideZapadReleased;
    private ChangeListener mutingCheckBoxListener;

    public Scene scena;
    public Stage staza = new Stage();
    private ArrayList<Semafor> listaSemafora = new ArrayList<>();
    private ArrayList<SemaforPjesaci> listaPjesaka = new ArrayList<>();
    private ArrayList<Timeline> listaAnimacijaSemafora = new ArrayList<>();
    private ArrayList<SimulacijaModel> listaVremenaSemafora = new ArrayList<>();
    private ArrayList<PathTransition> listaAnimacijaVozilaIstok = new ArrayList<>();
    private ArrayList<PathTransition> listaAnimacijaVozilaSjever = new ArrayList<>();
    private ArrayList<PathTransition> listaAnimacijaVozilaZapad = new ArrayList<>();
    public SimpleIntegerProperty odabirSemPrihvati = new SimpleIntegerProperty(1);
    public SimpleIntegerProperty otvorenostBind = new SimpleIntegerProperty();
    public SimpleIntegerProperty zatvorenostBind = new SimpleIntegerProperty();
   // public SimpleIntegerProperty odabirSemBind = new SimpleIntegerProperty(1);
    private Timeline referentnoVrijemeIstok = new Timeline();
    private Timeline referentnoVrijemeSjever = new Timeline();
    private Timeline referentnoVrijemeZapad = new Timeline();
    private Timeline animacijaSemafora;
    private Timeline referentnoVrijemeAutomatskiRezim;

    private Boolean pocetno = true;
    private Boolean iskljuciPjesake = true;
    private Boolean induktivnaPetljaIstokIsPressed = false;
    private Boolean induktivnaPetljaZapadIsPressed = false;
    private Boolean induktivnaPetljaSjeverIsPressed = false;

    private Group semafori, elementi_aplikacije;
    private TranslateTransition transition;
    private Boolean menuOpen = false;
    private boolean istokZapadRunning = false;
    private boolean sjeverRunning = false;
    private boolean zapadRunning = false;
    private ArrayList<Rectangle> vozilaIstok = new ArrayList<Rectangle>();
    private ArrayList<Rectangle> vozilaSjever = new ArrayList<Rectangle>();
    private ArrayList<Rectangle> vozilaZapad = new ArrayList<Rectangle>();
    private ArrayList<Integer> listaRedoslijedaZaustavljanjaIstok = new ArrayList<Integer>();
    private ArrayList<Integer> listaRedoslijedaZaustavljanjaSjever = new ArrayList<Integer>();
    private ArrayList<Integer> listaRedoslijedaZaustavljanjaZapad = new ArrayList<Integer>();
    private Duration vrijemeAnimacijeSemafora;

    private int velicinaListeAnimacijeVozilaIstok;
    private int velicinaListeAnimacijeVozilaSjever;
    private int velicinaListeAnimacijeVozilaZapad;
    AnimationTimer timerVozilaIstokRun, timerVozilaSjeverRun, timerVozilaSjeverStop, timerVozilaIstokStop, timerVozilaZapadRun, timerVozilaZapadStop;

    public SimulacijaController(SimulacijaModel myAppModel, SimulacijaMainView appMainView) {
        appM = myAppModel;
        appMainV = appMainView;

        appMainV.buildView();
        setEvents();
        appMainV.addMenuPressListener(listenerOnMenuPress);
        appMainV.addScenePressListener(listenerOnScenePress);
        scena = appMainV.getScena();

        appMainV.kreirajSemafore();
        dodajPocetnaVremenaSemaforima();

        elementi_aplikacije = ((Group) scena.getRoot());
        ubaciSemaforeUListu();

        elementi_aplikacije.getChildren().add(semafori);

        izradiListuAnimacijaSemafora();
        appMainV.addOnMouseReleasedPrihvatiListener(listenerPromjenePrihvati);
        appMainV.addOnMouseReleasedTipkalo(listenerTipkaloButton);

        appMainV.addOnMouseReleasedSliderListenerIstok(listenerOnSlideIstokReleased);
        appMainV.addOnMouseReleasedSliderListenerSjever(listenerOnSlideSjeverReleased);
        appMainV.addOnMouseReleasedSliderListenerZapad(listenerOnSlideZapadReleased);
        appMainV.addChangeListenerOnMutingCheckBox(mutingCheckBoxListener);

        appM.addListenerToModSelectionModelIstok(listenerOnChangeSlideVozilaIstok);
        appM.addListenerToModSelectionModelSjever(listenerOnChangeSlideVozilaSjever);
        appM.addListenerToModSelectionModelZapad(listenerOnChangeSlideVozilaZapad);

        appMainV.createGrupaVozila();

        ((Text) ((Group) elementi_aplikacije.getChildren().get(2)).getChildren().get(4)).textProperty().bind(appM.selektiranaVozilaIstok.asString().concat(" vozila"));
        ((Slider) ((Group) elementi_aplikacije.getChildren().get(2)).getChildren().get(5)).valueProperty().bindBidirectional(appM.selektiranaVozilaIstok);  // bind slidera  

        ((Text) ((Group) elementi_aplikacije.getChildren().get(2)).getChildren().get(6)).textProperty().bind(appM.selektiranaVozilaSjever.asString().concat(" vozila"));
        ((Slider) ((Group) elementi_aplikacije.getChildren().get(2)).getChildren().get(7)).valueProperty().bindBidirectional(appM.selektiranaVozilaSjever);  // bind slidera  

        ((Text) ((Group) elementi_aplikacije.getChildren().get(2)).getChildren().get(8)).textProperty().bind(appM.selektiranaVozilaZapad.asString().concat(" vozila"));
        ((Slider) ((Group) elementi_aplikacije.getChildren().get(2)).getChildren().get(9)).valueProperty().bindBidirectional(appM.selektiranaVozilaZapad);  // bind slidera  

        buildStage();

    }

    public final void setEvents() {
        listenerOnMenuPress = new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent t) {

                if (menuOpen == false) {
                    transition = TranslateTransitionBuilder.create()
                            .node(scena.getRoot().getChildrenUnmodifiable().get(2))
                            .fromY(0)
                            .toY(-303)
                            .duration(Duration.millis(700))
                            .interpolator(Interpolator.EASE_IN)
                            .cycleCount(1)
                            .build();
                    transition.play();
                    menuOpen = true;
                    appMainV.getIphoneMenu().toFront();
                }
            }
        };

        listenerOnScenePress = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                System.out.println("X: " + (me.getSceneX() - 330) + "Y: " + (me.getSceneY() - 170));  //skaliran je semafor, pa mu je pomaknuta pocetna tocka u koordinatnom sistemu
                System.out.println(" getx: " + me.getX() + "gety: " + me.getY());
            }
        };

        listenerOnSlideIstokReleased = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                if (!listaAnimacijaVozilaIstok.isEmpty()) {
                    manageranimacijaIstok();
                }
            }
        };
        listenerOnSlideSjeverReleased = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                if (!listaAnimacijaVozilaSjever.isEmpty()) {
                    managerAnimacijaSjever();
                }
            }
        };
        listenerOnSlideZapadReleased = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {

                managerAnimacijaZapad();
            }
        };

        listenerOnChangeSlideVozilaSjever = new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {

                int staraVrijednost = (Integer) t;
                int novaVrijednost = (Integer) t1;
                System.out.println("stara vrijednost: " + staraVrijednost);
                System.out.println("Nova vrijednost: " + novaVrijednost);

                if (staraVrijednost < novaVrijednost) {
                    for (int i = staraVrijednost; i < novaVrijednost; i++) {
                        PathTransition animacijaVozilaSjever = appMainV.kreirajAnimacijuVozilaSjever();

                        listaAnimacijaVozilaSjever.add(animacijaVozilaSjever);
                        vozilaSjever.add((Rectangle) animacijaVozilaSjever.getNode());

                        if (i != 0) {
                        }
                    }

                } else if (staraVrijednost > novaVrijednost) {
                    referentnoVrijemeSjever.stop();
                    timerVozilaSjeverRun.stop();
                    timerVozilaSjeverStop.stop();

                    for (int i = novaVrijednost; i < staraVrijednost; i++) {
                        System.out.println("i:" + i);
                        if (!listaAnimacijaVozilaSjever.isEmpty()) {
                            appMainV.grupaVozilaSjever.getChildren().remove((listaAnimacijaVozilaSjever.size()) - 1);
                            vozilaSjever.remove((listaAnimacijaVozilaSjever.size()) - 1);
                            listaAnimacijaVozilaSjever.remove((listaAnimacijaVozilaSjever.size()) - 1);
                        }

                    }

                    if (novaVrijednost == 0) {
                        sjeverRunning = false;
                        velicinaListeAnimacijeVozilaSjever = 0;
                        vozilaSjever.clear();
                        listaRedoslijedaZaustavljanjaSjever.clear();
                        listaAnimacijaVozilaSjever.clear();
                        induktivnaPetljaSjeverIsPressed = false;
                    }
                }
                velicinaListeAnimacijeVozilaSjever = listaAnimacijaVozilaSjever.size();
            }
        };

        listenerOnChangeSlideVozilaIstok = new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {

                int staraVrijednost = (Integer) t;
                int novaVrijednost = (Integer) t1;
                if (staraVrijednost < novaVrijednost) {
                    for (int i = staraVrijednost; i < novaVrijednost; i++) {
                        PathTransition animacijaVozilaIstok = appMainV.kreirajAnimacijuVozilaIstok();

                        listaAnimacijaVozilaIstok.add(animacijaVozilaIstok);
                        vozilaIstok.add((Rectangle) animacijaVozilaIstok.getNode());
                    }

                } else if (staraVrijednost > novaVrijednost) {
                    referentnoVrijemeIstok.stop();
                    timerVozilaIstokRun.stop();
                    timerVozilaIstokStop.stop();

                    for (int i = novaVrijednost; i < staraVrijednost; i++) {
                        if (!listaAnimacijaVozilaIstok.isEmpty()) {

                            appMainV.grupaVozilaIstok.getChildren().remove((listaAnimacijaVozilaIstok.size()) - 1);

                            vozilaIstok.remove((listaAnimacijaVozilaIstok.size() - 1));
                            listaAnimacijaVozilaIstok.remove((listaAnimacijaVozilaIstok.size()) - 1);

                        } else {

                        }

                    }

                    if (novaVrijednost == 0) {
                        istokZapadRunning = false;
                        velicinaListeAnimacijeVozilaIstok = 0;
                        vozilaIstok.clear();
                        listaRedoslijedaZaustavljanjaIstok.clear();
                        listaAnimacijaVozilaIstok.clear();
                        induktivnaPetljaIstokIsPressed = false;
                    }

                    System.out.println("elementi size:" + elementi_aplikacije.getChildren().size());
                    System.out.println("velicina liste animacije vozila:" + listaAnimacijaVozilaIstok.size());

                }
                velicinaListeAnimacijeVozilaIstok = listaAnimacijaVozilaIstok.size();

                System.out.println("listaanimacijavozila:" + listaAnimacijaVozilaIstok.size() + ",lista vozila:" + vozilaIstok.size());

            }
        };

        listenerOnChangeSlideVozilaZapad = new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {

                int staraVrijednost = (Integer) t;
                int novaVrijednost = (Integer) t1;
                System.out.println("stara vrijednost: " + staraVrijednost);
                System.out.println("Nova vrijednost: " + novaVrijednost);

                if (staraVrijednost < novaVrijednost) {
                    for (int i = staraVrijednost; i < novaVrijednost; i++) {
                        PathTransition animacijaVozilaZapad = appMainV.kreirajAnimacijuVozilaZapad();

                        listaAnimacijaVozilaZapad.add(animacijaVozilaZapad);
                        vozilaZapad.add((Rectangle) animacijaVozilaZapad.getNode());

                    }

                } else if (staraVrijednost > novaVrijednost) {

                    for (int i = novaVrijednost; i < staraVrijednost; i++) {
                        System.out.println("i:" + i);

                        if (!listaAnimacijaVozilaZapad.isEmpty()) {
                            appMainV.grupaVozilaZapad.getChildren().remove((listaAnimacijaVozilaZapad.size()) - 1);
                            vozilaZapad.remove((listaAnimacijaVozilaZapad.size() - 1));
                            listaAnimacijaVozilaZapad.remove((listaAnimacijaVozilaZapad.size()) - 1);
                            if (!(listaRedoslijedaZaustavljanjaZapad.isEmpty())) {

                                listaRedoslijedaZaustavljanjaZapad.remove(listaRedoslijedaZaustavljanjaZapad.size() - 1);

                            }

                        }

                    }
                    System.out.println("elementi size:" + elementi_aplikacije.getChildren().size());
                    System.out.println("velicina liste animacije vozila:" + listaAnimacijaVozilaZapad.size());

                }
                velicinaListeAnimacijeVozilaZapad = listaAnimacijaVozilaZapad.size();

                System.out.println("listaanimacijavozila:" + listaAnimacijaVozilaZapad.size() + ",lista vozila:" + vozilaZapad.size());

            }
        };

        listenerPromjenePrihvati = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
            }
        };

        listenerTipkaloButton = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {

                iskljuciPjesake = false;
            }
        };

        mutingCheckBoxListener = new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if (t1 == true) {
                    vrijemeAnimacijeSemafora = animacijaSemafora.getCurrentTime();
                    for (int i = 0; i < listaAnimacijaSemafora.size(); i++) {
                        listaAnimacijaSemafora.get(i).pause();
                    }

                    pokreniReferentnoVrijemeAutomatskiRezim();

                } else {
                    for (int i = 0; i < listaAnimacijaSemafora.size(); i++) {
                        listaAnimacijaSemafora.get(i).play();//ako stavimo od nule ne uhvati parametre definirane u nultoj sekundi

                    }

                    referentnoVrijemeAutomatskiRezim.stop();
                }

            }

        };

    }

    private void buildStage() {
        staza.setMaxWidth(1500);
        staza.setMaxHeight(800);
        staza.setScene(scena);
        staza.setResizable(true);
        staza.show();
    }

    public void dodajPocetnaVremenaSemaforima() {

        appM.setVrijemeOtvorenosti(BigDecimal.valueOf(10));
        appM.setVrijemeZatvorenosti(BigDecimal.valueOf(10));

        for (int i = 0; i < 4; i++) // za dodavanje u listu pocetnih vremena zatv i otv  za prva 4 semafora
        {
            listaVremenaSemafora.add(appM);

        }
        appM = new SimulacijaModel();
        appM.setVrijemeOtvorenosti(BigDecimal.valueOf(4));
        appM.setVrijemeZatvorenosti(BigDecimal.valueOf(13));

        listaVremenaSemafora.add(appM);

    }

    public void ubaciSemaforeUListu() {

        semafori = appMainV.getSemafori();

        listaSemafora.add((Semafor) semafori.getChildren().get(0));
        listaSemafora.add((Semafor) semafori.getChildren().get(1));
        listaSemafora.add((Semafor) semafori.getChildren().get(2));
        listaSemafora.add((Semafor) semafori.getChildren().get(3));
        listaSemafora.add((Semafor) semafori.getChildren().get(4));

        listaPjesaka.add((SemaforPjesaci) semafori.getChildren().get(5));
        listaPjesaka.add((SemaforPjesaci) semafori.getChildren().get(6));
        listaPjesaka.add((SemaforPjesaci) semafori.getChildren().get(7));
        listaPjesaka.add((SemaforPjesaci) semafori.getChildren().get(8));
        dodajIdSemaforima();

    }

    private void dodajIdSemaforima() {
        for (int i = 0; i < listaSemafora.size(); i++) {
            listaSemafora.get(i).setId(String.valueOf(i + 1));
        }
        for (int i = 0; i < listaPjesaka.size(); i++) {
            listaPjesaka.get(i).setId(String.valueOf(i + 6));
        }
    }

    public void izradiListuAnimacijaSemafora() {
        for (int i = 0; i < listaSemafora.size(); i++) {
            podesavanjeAnimacijaSemafora(i);
            listaAnimacijaSemafora.get(i).playFrom(Duration.seconds(-1));//ako stavimo od nule ne uhvati parametre definirane u nultoj sekundi
        }

    }

    private Duration formatiranjeVremena(int sekunde) {
        Duration vrijeme = Duration.seconds(sekunde);
        return vrijeme;
    }

    private void podesavanjeAnimacijaSemafora(int pozicijaSemafora) {

        animacijaSemafora = new Timeline();
        animacijaSemafora.setCycleCount(Timeline.INDEFINITE);

        int otvoreno;
        int zatvoreno;
        final int pozicija = pozicijaSemafora;
        if (pocetno == true) {
            otvoreno = listaVremenaSemafora.get(pozicija).getVrijemeOtvorenosti().intValue();
            zatvoreno = listaVremenaSemafora.get(pozicija).getVrijemeZatvorenosti().intValue();

        } else {
            otvoreno = otvorenostBind.get();
            zatvoreno = zatvorenostBind.get();

        }

        if (pozicija < 4) {

            KeyFrame zelenoGlavniPravac = new KeyFrame(Duration.ZERO, new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowBlinking(false);
                    listaSemafora.get(pozicija).setGreenOn(true);

                    listaSemafora.get(pozicija).setRedOn(false);
                    listaSemafora.get(pozicija).setYellowOn(false);

                    if (pozicija == 3) {
                        if (iskljuciPjesake == false) {
                            listaPjesaka.get(0).setRedOn(true);
                            listaPjesaka.get(0).setGreenOn(false);
                            listaPjesaka.get(1).setRedOn(true);
                            listaPjesaka.get(1).setGreenOn(false);

                        }

                    }

                }
            });

            KeyFrame zutoGlavniPravac = new KeyFrame(formatiranjeVremena(otvoreno), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowOn(true);

                    listaSemafora.get(pozicija).setGreenOn(false);

                    listaSemafora.get(pozicija).setRedOn(false);

                }
            });
            KeyFrame crvenoGlavniPravac = new KeyFrame(formatiranjeVremena(otvoreno + 2), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setRedOn(true);
                    listaSemafora.get(pozicija).setYellowOn(false);
                    listaSemafora.get(pozicija).setGreenOn(false);

                    if (pozicija == 3) {
                        if (iskljuciPjesake == false) {
                            listaPjesaka.get(0).setRedOn(false);
                            listaPjesaka.get(0).setGreenOn(true);
                            listaPjesaka.get(1).setRedOn(false);
                            listaPjesaka.get(1).setGreenOn(true);
                        }
                    }
                }
            });

            KeyFrame zutoCrvenoPocetakGlavniPravac = new KeyFrame(formatiranjeVremena(otvoreno + 2 + zatvoreno), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowOn(true);

                }
            });
            KeyFrame zutoCrvenoKrajGlavniPravac = new KeyFrame(formatiranjeVremena(otvoreno + 2 + zatvoreno + 1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowOn(false);
                    listaSemafora.get(pozicija).setRedOn(false);
                    listaSemafora.get(pozicija).setYellowOn(false);

                }
            });

            animacijaSemafora.getKeyFrames().addAll(zelenoGlavniPravac, zutoGlavniPravac, crvenoGlavniPravac, /*skretacNoviList, skretac,*/ zutoCrvenoPocetakGlavniPravac, zutoCrvenoKrajGlavniPravac);
        }

        if (pozicija == 4) {

            KeyFrame pocetno4SporedniPravac = new KeyFrame(formatiranjeVremena(0), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setRedOn(true);//za prvi put kad se pokrene animacija da bude crvenoGlavniPravac
                    listaSemafora.get(pozicija).setYellowBlinking(false);

                    if (iskljuciPjesake == false) {
                        listaPjesaka.get(pozicija - 1).setRedOn(false);
                        listaPjesaka.get(pozicija - 1).setGreenOn(true);
                        listaPjesaka.get(pozicija - 2).setRedOn(false);
                        listaPjesaka.get(pozicija - 2).setGreenOn(true);
                        iskljuciPjesake = true;
                    } else {
                        listaPjesaka.get(pozicija - 1).setRedOn(true);
                        listaPjesaka.get(pozicija - 2).setRedOn(true);
                        listaPjesaka.get(pozicija - 3).setRedOn(true);
                        listaPjesaka.get(pozicija - 4).setRedOn(true);

                    }
                }
            });
            KeyFrame pjesaciStop = new KeyFrame(formatiranjeVremena(zatvoreno), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                }
            });
            KeyFrame zutoCrvenoPocetakSporedniPravac = new KeyFrame(formatiranjeVremena(zatvoreno + 1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowOn(true);

                    listaPjesaka.get(pozicija - 1).setGreenOn(false);
                    listaPjesaka.get(pozicija - 1).setRedOn(true);
                    listaPjesaka.get(pozicija - 2).setGreenOn(false);
                    listaPjesaka.get(pozicija - 2).setRedOn(true);
                }
            });
            KeyFrame zutoCrvenoKrajSporedniPravac = new KeyFrame(formatiranjeVremena(zatvoreno + 2), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowOn(false);
                    listaSemafora.get(pozicija).setRedOn(false);
                    listaSemafora.get(pozicija).setGreenOn(true);

                    if (iskljuciPjesake == false) {
                        listaPjesaka.get(pozicija - 3).setRedOn(false);
                        listaPjesaka.get(pozicija - 4).setRedOn(false);
                        listaPjesaka.get(pozicija - 3).setGreenOn(true);
                        listaPjesaka.get(pozicija - 4).setGreenOn(true);
                        iskljuciPjesake = false;
                    }
                }
            });
            KeyFrame zelenoSporedniPravac = new KeyFrame(formatiranjeVremena(zatvoreno + otvoreno + 2), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setGreenOn(false);
                    listaSemafora.get(pozicija).setYellowOn(true);

                }
            });

            KeyFrame zutoSporedniPravac = new KeyFrame(formatiranjeVremena(zatvoreno + otvoreno + 4), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    listaSemafora.get(pozicija).setYellowOn(false);
                    listaSemafora.get(pozicija).setRedOn(true);
                }
            });
            KeyFrame crvenoSporedniPravac = new KeyFrame(listaAnimacijaSemafora.get(1).getCycleDuration(), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                }
            });

            animacijaSemafora.getKeyFrames().addAll(pocetno4SporedniPravac, pjesaciStop, zelenoSporedniPravac, zutoSporedniPravac, crvenoSporedniPravac, zutoCrvenoPocetakSporedniPravac, zutoCrvenoKrajSporedniPravac);

        }
        System.out.println(" listaAnimacija.add(pozicija, animacijaSemafora);" + pozicija);
        listaAnimacijaSemafora.add(pozicija, animacijaSemafora);

    }

    private void checkForCollisionGlavniPravac() {

        for (int i = 0; i < velicinaListeAnimacijeVozilaIstok; i++) {
            short vozilo1 = (short) vozilaIstok.get(i).getTranslateX();

            if (listaSemafora.get(0).isRedOn()) // zaustavljanje prvog vozilaIstok ispred semafora 
            {
                // povrsina ispred semafora u kojoj se vrsi provjera 
                if (vozilo1 < 530 && vozilo1 > 484) {
                    induktivnaPetljaIstokIsPressed = true;
                    if (listaAnimacijaVozilaIstok.get(i).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaIstok.get(i).pause();

                        listaRedoslijedaZaustavljanjaIstok.add(i);
                    }
                }
            }

            short vozilo2;
            if (i != velicinaListeAnimacijeVozilaIstok - 1) {
                vozilo2 = (short) vozilaIstok.get(i + 1).getTranslateX();
                if (abs(vozilo2 - vozilo1) < 40) {
                    if (listaAnimacijaVozilaIstok.get(i + 1).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaIstok.get(i + 1).pause();
                        listaRedoslijedaZaustavljanjaIstok.add(i + 1);
                    }
                }

            } else {  // kad je "i" zadnje vozilo u listi usporedjuje se sa prvim vozilom liste
                vozilo2 = (short) vozilaIstok.get(0).getTranslateX();
                if (abs(vozilo2 - vozilo1) < 50) {
                    if (listaAnimacijaVozilaIstok.get(0).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaIstok.get(0).pause();
                        listaRedoslijedaZaustavljanjaIstok.add(0);
                    }
                }
            }
        }
    }

    private void checkForCollisionSporedniPravac() {
        for (int i = 0; i < velicinaListeAnimacijeVozilaSjever; i++) {
            short vozilo1 = (short) vozilaSjever.get(i).getTranslateX();
            if (listaSemafora.get(4).isRedOn()) // zaustavljanje prvog vozilaIstok ispred semafora 
            {
                if (vozilo1 < 559 && vozilo1 > 515) // povrsina ispred semafora u kojoj se vrsi provjera 
                {
                    induktivnaPetljaSjeverIsPressed = true;
                    if (listaAnimacijaVozilaSjever.get(i).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaSjever.get(i).pause();

                        listaRedoslijedaZaustavljanjaSjever.add(i);
                    }
                }
            }
            short vozilo2;
            if (i != velicinaListeAnimacijeVozilaSjever - 1) {
                vozilo2 = (short) vozilaSjever.get(i + 1).getTranslateX();
                if (abs(vozilo2 - vozilo1) < 40) {
                    if (listaAnimacijaVozilaSjever.get(i + 1).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaSjever.get(i + 1).pause();
                        listaRedoslijedaZaustavljanjaSjever.add(i + 1);
                    }
                }

            } else {  // kad je "i" zadnje vozilo u listi usporedjuje se sa prvim vozilom liste
                vozilo2 = (short) vozilaSjever.get(0).getTranslateX();
                if (abs(vozilo2 - vozilo1) < 50) {
                    if (listaAnimacijaVozilaSjever.get(0).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaSjever.get(0).pause();
                        listaRedoslijedaZaustavljanjaSjever.add(0);
                    }
                }
            }
        }
    }

    private void checkForCollisionZapad() {
        for (int i = 0; i < velicinaListeAnimacijeVozilaZapad; i++) {

            //  
            //   {
            short vozilo1 = (short) vozilaZapad.get(i).getTranslateX();
            if (listaSemafora.get(3).isRedOn()) // zaustavljanje prvog vozilaIstok ispred semafora 
            {
                if (vozilo1 < 72 && vozilo1 > 42) // povrsina ispred semafora u kojoj se vrsi provjera 
                {
                    induktivnaPetljaZapadIsPressed = true;
                    if (listaAnimacijaVozilaZapad.get(i).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaZapad.get(i).pause();

                        listaRedoslijedaZaustavljanjaZapad.add(i);
                    }
                }
            }
            short vozilo2;
            if (i != velicinaListeAnimacijeVozilaZapad - 1) {
                vozilo2 = (short) vozilaZapad.get(i + 1).getTranslateX();
                if (abs(vozilo2 - vozilo1) < 40) {
                    if (listaAnimacijaVozilaZapad.get(i + 1).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaZapad.get(i + 1).pause();
                        listaRedoslijedaZaustavljanjaZapad.add(i + 1);
                    }
                }

            } else {  // kad je "i" zadnje vozilo u listi usporedjuje se sa prvim vozilom liste
                vozilo2 = (short) vozilaZapad.get(0).getTranslateX();
                if (abs(vozilo1 - vozilo2) < 50) {
                    if (listaAnimacijaVozilaZapad.get(0).getStatus().equals(Status.RUNNING)) {
                        listaAnimacijaVozilaZapad.get(0).pause();
                        listaRedoslijedaZaustavljanjaZapad.add(0);
                    }
                }
            }
        }
    }

    private void manageranimacijaIstok() {

        timerVozilaIstokRun = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                vozilaIstokRun();
            }
        };
        timerVozilaIstokRun.start();

        timerVozilaIstokStop = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                checkForCollisionGlavniPravac();   //  

            }
        };

        referentnoVrijemeIstok = TimelineBuilder.create()
                .keyFrames(
                        new KeyFrame(
                                new Duration(1000),
                                new EventHandler<ActionEvent>() {
                                    public void handle(javafx.event.ActionEvent t) {
                                        {

                                            if (istokZapadRunning == false) {
                                                int counterRunning = 0;
                                                for (int i = 0; i < velicinaListeAnimacijeVozilaIstok; i++) {
                                                    if (listaAnimacijaVozilaIstok.get(i).getStatus().equals(Status.RUNNING)) {
                                                        counterRunning++;   //zbroj running animacija

                                                    }
                                                }

                                                if (counterRunning == velicinaListeAnimacijeVozilaIstok) { // ako su sve animacije running
                                                    timerVozilaIstokRun.stop();

                                                    listaRedoslijedaZaustavljanjaIstok.clear();
                                                    timerVozilaIstokStop.start();
                                                    istokZapadRunning = true;
                                                }

                                            } else {
                                                if (listaSemafora.get(0).isYellowOn() && listaSemafora.get(0).isRedOn()) {
                                                    timerVozilaIstokStop.stop();

                                                    timerVozilaIstokRun.start();

                                                    istokZapadRunning = false;
                                                }
                                            }
                                        }
                                    }
                                })).cycleCount(Timeline.INDEFINITE)
                .build();

        referentnoVrijemeIstok.play();

    }

    private void managerAnimacijaSjever() {

        timerVozilaSjeverRun = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                vozilaSjeverRun();

            }
        };
        timerVozilaSjeverRun.start();

        timerVozilaSjeverStop = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                checkForCollisionSporedniPravac();   //  
            }
        };

        referentnoVrijemeSjever = TimelineBuilder.create()
                .keyFrames(
                        new KeyFrame(
                                new Duration(1000),
                                new EventHandler<ActionEvent>() {
                                    public void handle(javafx.event.ActionEvent t) {
                                        {

                                            if (sjeverRunning == false) {
                                                int counterRunning = 0;

                                                for (int i = 0; i < velicinaListeAnimacijeVozilaSjever; i++) {
                                                    if (listaAnimacijaVozilaSjever.get(i).getStatus().equals(Status.RUNNING)) {
                                                        counterRunning++;   //zbroj running animacija
                                                    }
                                                }

                                                if (counterRunning == velicinaListeAnimacijeVozilaSjever) { // ako su sve animacije running
                                                    timerVozilaSjeverRun.stop();

                                                    listaRedoslijedaZaustavljanjaSjever.clear();

                                                    timerVozilaSjeverStop.start();
                                                    sjeverRunning = true;
                                                }

                                            } else {
                                                if (listaSemafora.get(4).isYellowOn() && listaSemafora.get(0).isRedOn()) {
                                                    timerVozilaSjeverStop.stop();

                                                    timerVozilaSjeverRun.start();

                                                    sjeverRunning = false;
                                                }
                                            }
                                        }
                                    }
                                })).cycleCount(Timeline.INDEFINITE)
                .build();

        referentnoVrijemeSjever.play();

    }

    private void managerAnimacijaZapad() {

        timerVozilaZapadRun = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                vozilaZapadRun();
            }
        };
        timerVozilaZapadRun.start();

        timerVozilaZapadStop = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                checkForCollisionZapad();
            }
        };

        referentnoVrijemeZapad = TimelineBuilder.create()
                .keyFrames(
                        new KeyFrame(
                                new Duration(1000),
                                new EventHandler<ActionEvent>() {
                                    public void handle(javafx.event.ActionEvent t) {
                                        {

                                            if (zapadRunning == false) {
                                                int counterRunning = 0;

                                                for (int i = 0; i < velicinaListeAnimacijeVozilaZapad; i++) {
                                                    if (listaAnimacijaVozilaZapad.get(i).getStatus().equals(Status.RUNNING)) {
                                                        counterRunning++;   //zbroj running animacija
                                                    }
                                                }

                                                if (counterRunning == velicinaListeAnimacijeVozilaZapad) { // ako su sve animacije running
                                                    timerVozilaZapadRun.stop();

                                                    listaRedoslijedaZaustavljanjaZapad.clear();

                                                    timerVozilaZapadStop.start();
                                                    zapadRunning = true;
                                                }

                                            } else {
                                                if (listaSemafora.get(3).isYellowOn() && listaSemafora.get(3).isRedOn()) {
                                                    timerVozilaZapadStop.stop();

                                                    timerVozilaZapadRun.start();

                                                    zapadRunning = false;
                                                }
                                            }
                                        }
                                    }
                                })).cycleCount(Timeline.INDEFINITE)
                .build();

        referentnoVrijemeZapad.play();

    }

    private void vozilaIstokRun() {

        if (!listaRedoslijedaZaustavljanjaIstok.isEmpty()) {   // nije prvo pokretanje 
            if (listaSemafora.get(0).isGreenOn()) {
                if (induktivnaPetljaIstokIsPressed == true) {
                    induktivnaPetljaIstokIsPressed = false;

                }

                PathTransition animacijavozila1 = listaAnimacijaVozilaIstok.get(listaRedoslijedaZaustavljanjaIstok.get(0));
                if (animacijavozila1.getStatus().equals(Status.PAUSED)) {
                    animacijavozila1.play();

                } else {

                    short vozilo1 = (short) vozilaIstok.get(listaRedoslijedaZaustavljanjaIstok.get(0)).getTranslateX();

                    short vozilo2 = (short) vozilaIstok.get(listaRedoslijedaZaustavljanjaIstok.get(1)).getTranslateX();
                    if ((vozilo2 - vozilo1) > 60) {

                        PathTransition animacijavozila2 = listaAnimacijaVozilaIstok.get(listaRedoslijedaZaustavljanjaIstok.get(1));
                        if (animacijavozila2.getStatus().equals(Status.PAUSED)) {
                            if (listaRedoslijedaZaustavljanjaIstok.size() > 2) {
                                animacijavozila2.play();
                                listaRedoslijedaZaustavljanjaIstok.remove(0);
                            } else {
                                if (animacijavozila2.getStatus().equals(Status.PAUSED)) {
                                    animacijavozila2.play();
                                }
                            }
                        }
                    }
                }

            }
        } else {
            if (!listaAnimacijaVozilaIstok.isEmpty()) {
                if (!listaAnimacijaVozilaIstok.get(0).getStatus().equals(Status.RUNNING)) {
                    listaAnimacijaVozilaIstok.get(0).play();
                    vozilaIstok.get(0).setVisible(true);
                }

                for (int i = 1; i < velicinaListeAnimacijeVozilaIstok; i++) {

                    if (listaAnimacijaVozilaIstok.get(i).getStatus().equals(Status.STOPPED)) {
                        short vozilo1 = (short) vozilaIstok.get(i - 1).getTranslateX();
                        short vozilo2 = (short) vozilaIstok.get(i).getTranslateX();
                        if ((vozilo2 - vozilo1) > 80) {
                            listaAnimacijaVozilaIstok.get(i).play();

                        }
                    }
                }
            }
        }
    }

    private void vozilaSjeverRun() {

        if (!listaRedoslijedaZaustavljanjaSjever.isEmpty()) {   // nije prvo pokretanje 
            if (listaSemafora.get(4).isGreenOn()) {
                if (induktivnaPetljaSjeverIsPressed == true) {
                    induktivnaPetljaSjeverIsPressed = false;

                }
                PathTransition animacijavozila1 = listaAnimacijaVozilaSjever.get(listaRedoslijedaZaustavljanjaSjever.get(0));
                if (animacijavozila1.getStatus().equals(Status.PAUSED)) {
                    animacijavozila1.play();

                } else {
                    short vozilo1 = (short) vozilaSjever.get(listaRedoslijedaZaustavljanjaSjever.get(0)).getTranslateX();
                    short vozilo2 = (short) vozilaSjever.get(listaRedoslijedaZaustavljanjaSjever.get(1)).getTranslateX();
                    if ((vozilo2 - vozilo1) > 70) {

                        PathTransition animacijavozila2 = listaAnimacijaVozilaSjever.get(listaRedoslijedaZaustavljanjaSjever.get(1));
                        if (animacijavozila2.getStatus().equals(Status.PAUSED)) {
                            if (listaRedoslijedaZaustavljanjaSjever.size() > 2) {
                                animacijavozila2.play();
                                listaRedoslijedaZaustavljanjaSjever.remove(0);
                            } else {
                                if (animacijavozila2.getStatus().equals(Status.PAUSED)) {
                                    animacijavozila2.play();
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (!listaAnimacijaVozilaSjever.isEmpty()) {
                if (!listaAnimacijaVozilaSjever.get(0).getStatus().equals(Status.RUNNING)) {
                    listaAnimacijaVozilaSjever.get(0).play();
                    vozilaSjever.get(0).setVisible(true);
                }

                for (int i = 1; i < velicinaListeAnimacijeVozilaSjever; i++) {
                    if (listaAnimacijaVozilaSjever.get(i).getStatus().equals(Status.STOPPED)) {
                        short vozilo1 = (short) vozilaSjever.get(i - 1).getTranslateX();
                        short vozilo2 = (short) vozilaSjever.get(i).getTranslateX();
                        if ((vozilo2 - vozilo1) > 100) {
                            listaAnimacijaVozilaSjever.get(i).play();
                            vozilaSjever.get(i).setVisible(true);
                        }
                    }
                }
            }
        }
    }

    private void vozilaZapadRun() {
        if (!listaRedoslijedaZaustavljanjaZapad.isEmpty()) {   // nije prvo pokretanje 

            if (listaSemafora.get(3).isGreenOn()) {
                if (induktivnaPetljaZapadIsPressed == true) {
                    induktivnaPetljaZapadIsPressed = false;
                }
                PathTransition animacijavozila1 = listaAnimacijaVozilaZapad.get(listaRedoslijedaZaustavljanjaZapad.get(0));
                if (animacijavozila1.getStatus().equals(Status.PAUSED)) {
                    animacijavozila1.play();

                } else {
                    short vozilo1 = (short) vozilaZapad.get(listaRedoslijedaZaustavljanjaZapad.get(0)).getTranslateX();
                    short vozilo2 = (short) vozilaZapad.get(listaRedoslijedaZaustavljanjaZapad.get(1)).getTranslateX();
                    if ((vozilo1 - vozilo2) > 60) {
                        PathTransition animacijavozila2 = listaAnimacijaVozilaZapad.get(listaRedoslijedaZaustavljanjaZapad.get(1));
                        if (animacijavozila2.getStatus().equals(Status.PAUSED)) {
                            if (listaRedoslijedaZaustavljanjaZapad.size() > 2) {
                                animacijavozila2.play();
                                listaRedoslijedaZaustavljanjaZapad.remove(0);
                            } else {
                                if (animacijavozila2.getStatus().equals(Status.PAUSED)) {
                                    animacijavozila2.play();
                                }
                            }
                        }
                    }
                }
            }

        } else {  // prvo pokretanje vozilaIstok
            if (!listaAnimacijaVozilaZapad.isEmpty()) {
                if (!listaAnimacijaVozilaZapad.get(0).getStatus().equals(Status.RUNNING)) {
                    listaAnimacijaVozilaZapad.get(0).play();
                    vozilaZapad.get(0).setVisible(true);
                }

                for (int i = 1; i < velicinaListeAnimacijeVozilaZapad; i++) {
                    if (listaAnimacijaVozilaZapad.get(i).getStatus().equals(Status.STOPPED)) {
                        short vozilo1 = (short) vozilaZapad.get(i - 1).getTranslateX();
                        short vozilo2 = (short) vozilaZapad.get(i).getTranslateX();
                        if ((vozilo1 - vozilo2) > 80) {
                            listaAnimacijaVozilaZapad.get(i).play();
                            vozilaZapad.get(i).setVisible(true);
                        }
                    }
                }
            }
        }
    }

    private void pokreniReferentnoVrijemeAutomatskiRezim() {
        referentnoVrijemeAutomatskiRezim = TimelineBuilder.create()
                .keyFrames(
                        new KeyFrame(
                                new Duration(1000),
                                new EventHandler<ActionEvent>() {
                                    public void handle(javafx.event.ActionEvent t) {

                                        if (induktivnaPetljaSjeverIsPressed == true) {
                                            System.out.println(listaAnimacijaVozilaSjever.size());

                                            if (!listaAnimacijaVozilaSjever.isEmpty()) {
                                                for (int i = 0; i < listaAnimacijaSemafora.size(); i++) {
                                                    listaAnimacijaSemafora.get(i).play();

                                                }
                                            }
                                        } else {
                                            for (int i = 0; i < listaAnimacijaSemafora.size(); i++) {

                                                listaAnimacijaSemafora.get(i).pause();
                                            }
                                        }

                                        if (induktivnaPetljaIstokIsPressed == true) {
                                            if (!listaAnimacijaVozilaIstok.isEmpty()) {
                                                for (int i = 0; i < listaAnimacijaSemafora.size(); i++) {
                                                    // System.out.println(listaAnimacijaSemafora.get(i).getCurrentTime());
                                                    listaAnimacijaSemafora.get(i).play();
                                                }
                                            }

                                        }

                                        if (induktivnaPetljaZapadIsPressed == true) {
                                            for (int i = 0; i < listaAnimacijaSemafora.size(); i++) {
                                                // System.out.println(listaAnimacijaSemafora.get(i).getCurrentTime());
                                                listaAnimacijaSemafora.get(i).play();

                                            }
                                        }

                                    }

                                })).cycleCount(Timeline.INDEFINITE)
                .build();

        referentnoVrijemeAutomatskiRezim.play();

    }

}
