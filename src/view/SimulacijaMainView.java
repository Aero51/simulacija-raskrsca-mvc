    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.javafx.experiments.scenicview.ScenicView;
import java.util.Random;
import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.GroupBuilder;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckBoxBuilder;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceBoxBuilder;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SliderBuilder;
import javafx.scene.control.ToolBarBuilder;
import javafx.scene.image.Image;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradientBuilder;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineBuilder;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;
import javafx.util.Duration;

import jfxtras.labs.scene.control.gauge.Semafor;
import jfxtras.labs.scene.control.gauge.SemaforBuilder;
import jfxtras.labs.scene.control.gauge.SemaforPjesaci;
import jfxtras.labs.scene.control.gauge.SemaforPjesaciBuilder;
import jfxtras.labs.scene.control.gauge.Skretaci;
import jfxtras.labs.scene.control.gauge.SkretaciBuilder;
import model.SimulacijaModel;

/**
 *
 * @author Nikola
 */
public class SimulacijaMainView {

    private SimulacijaModel appM;
    private Group grupa_elementi;

    private Group iphoneMenu, semafori, grupaNumberSpinners;
    public Group grupaVozilaIstok, grupaVozilaSjever, grupaVozilaZapad;
    private Scene scena;
    private Rectangle klikabilno;
    private SemaforBuilder semaforBuilder;
    private SemaforPjesaciBuilder pjesaciBuilder;
    private SkretaciBuilder skretaciBuilder;

    private Text textBrojVozilaIstok, textBrojVozilaSjever, textBrojVozilaZapad;
    private Slider sliderVozilaIstok;
    private Slider sliderVozilaSjever;
    private Slider sliderVozilaZapad;

    private Button prihvati, tipkaloButton;
    public IntegerProperty bindingSemafora = new SimpleIntegerProperty(1);
    public SimpleIntegerProperty odabirSemPrihvati = new SimpleIntegerProperty(1);
    public SimpleIntegerProperty odabirBind = new SimpleIntegerProperty(1);
    public SimpleIntegerProperty otvorenostBind = new SimpleIntegerProperty();
    public SimpleIntegerProperty zatvorenostBind = new SimpleIntegerProperty();
    private CheckBox automatskiRezimCheckBox;
    public SimulacijaMainView(SimulacijaModel myAppModel) {

        appM = myAppModel;

    }

    public Group getGrupaVozilaIstok() {
        return grupaVozilaIstok;
    }

    public Group getGrupaVozilaSjever() {
        return grupaVozilaSjever;
    }

    public Group getIphoneMenu() {
        return iphoneMenu;
    }

    public Group getSemafori() {
        return semafori;
    }

    public Group getGrupaNumberSpinners() {
        return grupaNumberSpinners;  
    }

    public Scene getScena() {
        return scena;
    }

    public void buildView() {
        createGrupaElementiAplikacije();
        scena = new Scene(grupa_elementi, 900, 500);
          ScenicView.show(scena);
    
    }

 


    public void addScenePressListener(EventHandler<MouseEvent> listener) {
        scena.setOnMouseClicked(listener);
    }

    public void addMenuPressListener(EventHandler<MouseEvent> listener) {
        klikabilno.setOnMouseClicked(listener);
    }

    public void addOnMouseReleasedSliderListenerIstok(EventHandler<MouseEvent> listener) {
        sliderVozilaIstok.setOnMouseReleased(listener);
    }

    public void addOnMouseReleasedSliderListenerSjever(EventHandler<MouseEvent> listener) {
        sliderVozilaSjever.setOnMouseReleased(listener);
    }

    public void addOnMouseReleasedSliderListenerZapad(EventHandler<MouseEvent> listener) {
        sliderVozilaZapad.setOnMouseReleased(listener);
    }

    public void addOnMouseReleasedPrihvatiListener(EventHandler<MouseEvent> listener) {
        prihvati.setOnMouseReleased(listener);
    }

    public void addOnMouseReleasedTipkalo(EventHandler<MouseEvent> listener) {
        tipkaloButton.setOnMouseReleased(listener);
    }

    public void addChangeListenerOnSliderIstok(EventHandler<MouseEvent> listener) {

        sliderVozilaIstok.setOnMouseDragged(listener);   // ne koristi se ,  u modelu je 
    }

    public void addChangeListenerOnMutingCheckBox(ChangeListener listener) {

        automatskiRezimCheckBox.selectedProperty().addListener(listener);

    }

    private void kreirajSemaforBuildere() {
        semaforBuilder = SemaforBuilder.create()
                .darkBackground(true)
                .prefHeight(40)
                .prefWidth(16);

        pjesaciBuilder = SemaforPjesaciBuilder.create()
                .prefHeight(40)
                .prefWidth(16);

     

    }

    public void kreirajSemafore() {
        kreirajSemaforBuildere();
        Semafor semafor01 = semaforBuilder.layoutX(492).layoutY(291).build();
        semafor01.setRotate(300);
        Semafor semafor02 = semaforBuilder.layoutX(466).layoutY(344).build();
        semafor02.setRotate(210);

        Semafor semafor03 = semaforBuilder.layoutX(40).layoutY(170).build();
        semafor03.setRotate(130);
        Semafor semafor04 = semaforBuilder.layoutX(91).layoutY(130).build();
        semafor04.setRotate(30);
        Semafor semafor05 = semaforBuilder.layoutX(492).layoutY(29).build();
        semafor05.setRotate(270);

        SemaforPjesaci semafor06 = pjesaciBuilder.layoutX(461).layoutY(282).build();
        semafor06.setDarkBackground(true);
        semafor06.setRotate(30);

        SemaforPjesaci semafor07 = pjesaciBuilder.layoutX(385).layoutY(395).build();
        semafor07.setRotate(210);
        SemaforPjesaci semafor08 = pjesaciBuilder.layoutX(459).layoutY(39).build();
        semafor08.setRotate(170);
        SemaforPjesaci semafor09 = pjesaciBuilder.layoutX(470).layoutY(135).build();
        semafor09.setRotate(170);
        
        semafori = GroupBuilder.create().children(semafor01, semafor02, semafor03, semafor04, semafor05, semafor06, semafor07, semafor08, semafor09/*, semafor10, semafor11, semafor12*//*, semafor13, semafor14,semafor15*/).build();
    }

    private void createGrupaElementiAplikacije() {

        ChoiceBox genreChoiceBox;
        Rectangle bijelaPozadina;
        

        grupa_elementi = GroupBuilder.create()
                .children(
                        ImageViewBuilder.create()
                        .image(new Image("zadnje.png"))
                        .build(),
                        tipkaloButton = ButtonBuilder.create().text("Tipkalo").id("Tipkalo").layoutX(477).layoutY(211).build(),
                        iphoneMenu = GroupBuilder.create() // ova grupa je dijete gornje grupe
                        .layoutX(585)
                        .layoutY(434)
                        .children(
                                klikabilno = RectangleBuilder.create()
                                .width(320) //gornji pravokutnik
                                .height(45)
                                .arcWidth(20)
                                .arcHeight(20)
                                .fill(
                                        LinearGradientBuilder.create()
                                        .endX(0.0)
                                        .endY(1.0)
                                        .stops(
                                                new Stop(0, Color.web("0xAEBBCC")),
                                                new Stop(1, Color.web("0x6D84A3")))
                                        .build())
                                .build(),
                                TextBuilder.create()
                                .layoutX(65)
                                .layoutY(12)
                                .textOrigin(VPos.TOP)
                                .fill(Color.WHITE)
                                .mouseTransparent(true)
                                .text("Izbornik")
                                .font(Font.font("SansSerif", FontWeight.BOLD, 20))
                                .build(),
                                RectangleBuilder.create()
                                .x(0)
                                .y(43)
                                .width(320)
                                .height(344)
                                .fill(Color.rgb(199, 206, 213))
                                .build(),
                                bijelaPozadina = RectangleBuilder.create()
                                .x(9)
                                .y(54)
                                .width(300)
                                .height(338) 
                                .arcWidth(20)
                                .arcHeight(20)
                                .fill(Color.WHITE)
                                .stroke(Color.color(0.66, 0.67, 0.69))
                                .build(),
                                textBrojVozilaIstok = TextBuilder.create()
                                .layoutX(18)
                                .layoutY(154) 
                                .textOrigin(VPos.TOP)
                                .text("vozila i")
                                .fill(Color.web("#131021"))
                                .font(Font.font("SansSerif", FontWeight.BOLD, 18))
                                .build(),
                                sliderVozilaIstok = SliderBuilder.create()
                                .layoutX(170)
                                .layoutY(154)  
                                .prefWidth(100)
                                .min(appM.minVozila)
                                .max(appM.maxVozila)
                                .build(),
                                textBrojVozilaSjever = TextBuilder.create()
                                .layoutX(18)
                                .layoutY(242) 
                                .textOrigin(VPos.TOP)
                                .text("vozila sj")
                                .fill(Color.web("#131021"))
                                .font(Font.font("SansSerif", FontWeight.BOLD, 18))
                                .build(),
                                sliderVozilaSjever = SliderBuilder.create()
                                .layoutX(170)
                                .layoutY(242)   
                                .prefWidth(100)
                                .min(appM.minVozila)
                                .max(appM.maxVozila)
                                .build(),
                                textBrojVozilaZapad = TextBuilder.create()
                                .layoutX(18)
                                .layoutY(342) 
                                .textOrigin(VPos.TOP)
                                .text("vozila z")
                                .fill(Color.web("#131021"))
                                .font(Font.font("SansSerif", FontWeight.BOLD, 18))
                                .build(),
                                sliderVozilaZapad = SliderBuilder.create()
                                .layoutX(170)
                                .layoutY(342)   
                                .prefWidth(100)
                                .min(appM.minVozila)
                                .max(appM.maxVozila)
                                .build(),
                              
                                LineBuilder.create()
                                .startX(9)
                                .startY(97)
                                .endX(309)
                                .endY(97)
                                .stroke(Color.color(0.66, 0.67, 0.69))
                                .build(),
                                TextBuilder.create()
                                .layoutX(18)
                                .layoutY(66) 
                                .textOrigin(VPos.TOP)
                                .fill(Color.web("#131021"))
                                .text("Automatski režim")
                                .font(Font.font("SanSerif", FontWeight.BOLD, 18))
                                .build(),
                                automatskiRezimCheckBox= CheckBoxBuilder.create()
                                .layoutX(280)
                                .layoutY(66) 
                                .build(),
                               
                                  TextBuilder.create()
                                 .layoutX(18)
                                 .layoutY(110) 
                                 .textOrigin(VPos.TOP)
                                 .fill(Color.web("#131021"))
                                 .text("Vozila istok:")
                                 .font(Font.font("SanSerif", FontWeight.BOLD, 18))
                                 .build(),
                                  TextBuilder.create()
                                 .layoutX(18)
                                 .layoutY(198) 
                                 .textOrigin(VPos.TOP)
                                 .fill(Color.web("#131021"))
                                 .text("Vozila sjever:")
                                 .font(Font.font("SanSerif", FontWeight.BOLD, 18))
                                 .build(),
                                  TextBuilder.create()
                                 .layoutX(18)
                                 .layoutY(286) // .layoutY(66)
                                 .textOrigin(VPos.TOP)
                                 .fill(Color.web("#131021"))
                                 .text("Vozila zapad:")
                                 .font(Font.font("SanSerif", FontWeight.BOLD, 18))
                                 .build(),
                                LineBuilder.create()
                                .startX(9)
                                .startY(185) //   44 je razlika do sljedece crte
                                .endX(309)
                                .endY(185)
                                .stroke(Color.color(0.66, 0.67, 0.69))
                                .build(),
                               
                                LineBuilder.create() 
                                .startX(9)
                                .startY(273) //   44 je razlika do sljedece crte
                                .endX(309)
                                .endY(273)
                                .stroke(Color.color(0.66, 0.67, 0.69))
                                .build(),
                                ToolBarBuilder.create()
                                .id("iphone-toolbar")
                                .layoutX(10)
                                .layoutY(373)
                                .stylesheets(SimulacijaMainView.class.getResource("Buttons.css").toExternalForm())
                                .items(
                                        prihvati = ButtonBuilder.create().text("Prihvati").id("iphone").build())
                                .build())
                        .build())
                .build();

    }

    public void createGrupaVozila() {
        grupaVozilaIstok = GroupBuilder.create().build();
        grupaVozilaSjever = GroupBuilder.create().build();
        grupaVozilaZapad = GroupBuilder.create().build();

        grupa_elementi.getChildren().add(grupaVozilaIstok);
        grupa_elementi.getChildren().add(grupaVozilaSjever);
        grupa_elementi.getChildren().add(grupaVozilaZapad);
        System.out.println("velicina poslije dodavanja 2 grupe vozila" + grupa_elementi.getChildren().size());

    }

    public PathTransition kreirajAnimacijuVozilaIstok() {

        Rectangle vozilo = kreirajVozilo();
        vozilo.setTranslateX(873);  // inace dok se vozilo ne pokrene vidljivo je na koordinati x : 0 

        grupaVozilaIstok.getChildren().add(vozilo);
        Path putIstok = kreirajPutIstok();
        PathTransition transitionIstok = generatePathTransition(vozilo, putIstok);
        return transitionIstok;
    }

    public PathTransition kreirajAnimacijuVozilaZapad() {
        Rectangle vozilo = kreirajVozilo();
        vozilo.setTranslateX(-40);
        grupaVozilaZapad.getChildren().add(vozilo);
        Path putZapad = kreirajPutZapad();
        PathTransition transitionZapad = generatePathTransition(vozilo, putZapad);
        return transitionZapad;
    }

    public PathTransition kreirajAnimacijuVozilaSjever() {
        Rectangle vozilo = kreirajVozilo();
        vozilo.setTranslateX(892);
        grupaVozilaSjever.getChildren().add(vozilo);
        Path putSjever = kreirajPutSjever();
        PathTransition transitionSjever = generatePathTransition(vozilo, putSjever);
        return transitionSjever;

    }

    private Rectangle kreirajVozilo() {
        final Rectangle vozilo = new Rectangle(40, 20);
 Random r = new Random();
        r.nextInt(256);
        int rCol1 = r.nextInt(256);
        int rCol2 = r.nextInt(256);
        int rCol3 = r.nextInt(256);
        vozilo.setFill(Color.rgb(rCol1, rCol2, rCol3));

        return vozilo;
    }

    private Path kreirajPutIstok() {

        Path put = new Path();

        put.getElements().add(new MoveTo(873, 566));
        put.getElements().add(new LineTo(703, 466));
        put.getElements().add(new LineTo(542, 375));
        put.getElements().add(new LineTo(396, 287));
        put.getElements().add(new LineTo(275, 204));
        put.getElements().add(new LineTo(159, 118));
        put.getElements().add(new LineTo(18, 4));
        put.getElements().add(new LineTo(-50, -50));

       
        return put;

    }

    private Path kreirajPutZapad() {

        Path put = new Path();

        put.getElements().add(new MoveTo(0, 117));
        put.getElements().add(new LineTo(53, 157));
        put.getElements().add(new LineTo(279, 324));
        put.getElements().add(new LineTo(453, 436));
        put.getElements().add(new LineTo(530, 477));
        put.getElements().add(new LineTo(697, 574));

        return put;

    }

    private Path kreirajPutSjever() {

        Path putSjever = new Path();

        putSjever.getElements().add(new MoveTo(892, 14));
        putSjever.getElements().add(new LineTo(543, 82));
        putSjever.getElements().add(new LineTo(377, 98));
        putSjever.getElements().add(new LineTo(288, 93));
        putSjever.getElements().add(new LineTo(169, 74));
        putSjever.getElements().add(new LineTo(58, 35));
        putSjever.getElements().add(new LineTo(14, 0));

        return putSjever;

    }

    private PathTransition generatePathTransition(final Shape shape, final Path path) {
        final PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.seconds(6.0));

        pathTransition.setPath(path);
        pathTransition.setNode(shape);
        pathTransition.setInterpolator(Interpolator.LINEAR);

        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setCycleCount(Timeline.INDEFINITE);

        return pathTransition;
    }

}
